"use strict";
const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log("Welcome to the Chiwaukee generator!");

    const prompts = [
      {
        type: "text",
        name: "destinationPath",
        message: "Where is this project going?",
        default: "./"
      },
      {
        type: "text",
        name: "name",
        message: "What should we name this project?",
        default: 'default name'
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath("./"),
      this.destinationPath(this.props.destinationPath),
      { ...this.props }
    );
    this.fs.copyTpl(
      this.templatePath("./.*"),
      this.destinationPath(this.props.destinationPath),
      { ...this.props }
    );
  }
};
