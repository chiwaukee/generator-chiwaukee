# generator-chiwaukee

## Installation

First, install [Yeoman](http://yeoman.io) and generator-chiwaukee using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-chiwaukee
```

Then generate your new project:

```bash
yo chiwaukee
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

ISC © []()


[npm-image]: https://badge.fury.io/js/generator-chiwaukee.svg
[npm-url]: https://npmjs.org/package/generator-chiwaukee
[travis-image]: https://travis-ci.com//generator-chiwaukee.svg?branch=master
[travis-url]: https://travis-ci.com//generator-chiwaukee
[daviddm-image]: https://david-dm.org//generator-chiwaukee.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-chiwaukee
