## [1.3.4](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.3.3...v1.3.4) (2022-09-03)


### Bug Fixes

* add lint to gitlab-ci ([a73a933](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/a73a933bf83a6f11482c365d1e57fb616b73569e))

## [1.3.3](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.3.2...v1.3.3) (2022-09-03)


### Bug Fixes

* index and dependencies ([bb6a6a9](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/bb6a6a9c5c5f6d0aee71f3daddc9394b3c622519))

## [1.3.2](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.3.1...v1.3.2) (2022-09-03)


### Bug Fixes

* remove analytics ([c35ea5a](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/c35ea5a9a656decf6a6c4aa26dbe2da31e87ac85))

## [1.3.1](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.3.0...v1.3.1) (2022-09-03)


### Bug Fixes

* remove faker ([906012d](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/906012d483af8ef90f37a0f562002453cc7edc6b))

## [1.3.0](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.2.0...v1.3.0) (2022-09-03)


### Features

* add error boundary ([06118f2](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/06118f27ce105f627f4bfac77d0327fa4772b1c3))

## [1.2.0](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.1.1...v1.2.0) (2022-09-03)


### Features

* add more features to template ([6cc5ccc](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/6cc5ccc09ccd10f3426bb7473461cf2715e9ae73))


### Bug Fixes

* refresh webpack config ([1aedf50](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/1aedf50635a24a6a0bf068f2330732fa7966a15e))
* refresh webpack config ([1bbce41](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/1bbce4116daae6236d66402999e83ef0b11c1f85))

## [1.1.1](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.1.0...v1.1.1) (2022-08-27)


### Bug Fixes

* vulnerabilties ([f24b224](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/f24b224585d9e62ace2d10a0e3094d0af162d04e))

# [1.1.0](https://gitlab.com/chiwaukee/generator-chiwaukee/compare/v1.0.0...v1.1.0) (2022-08-27)


### Features

* initial commit ([4ea0898](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/4ea0898d09121541494566d08d16d2c1268b373f))
* update readme ([344ad7c](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/344ad7c2a0edf317b67820232d32640ae523bbce))

# 1.0.0 (2022-08-27)


### Features

* initial commit ([7a50a7b](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/7a50a7bcee482021469d69dbefcb169f8b4240b1))
* initial commit ([8d67c77](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/8d67c77142f1ac527e5607d727fe51c38d17f35f))
* initial commit ([f953f69](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/f953f695cac93d7b397c797daf91c902674b0ad1))
* initial commit ([b204e4e](https://gitlab.com/chiwaukee/generator-chiwaukee/commit/b204e4e2b681b4f40a48e95e7cc54f05c9a73ed5))
